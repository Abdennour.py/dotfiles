/* 8 normal colors */
static const char black[]		= "#1d1f21";
static const char red[]			= "#b24040"; 
static const char green[] 		= "#2ebe8f";
static const char yellow[]		= "#dc8531";
static const char blue[] 		= "#518fd8";
static const char magenta[]		= "#865596";
static const char cyan[]  		= "#72b7c9";
static const char white[] 		= "#707880";

/* 8 bright colors */
static const char b_black[]		= "#373b41";
static const char b_red[]		= "#cc6666"; 
static const char b_green[] 	= "#b5bd68";
static const char b_yellow[]	= "#f0c674";
static const char b_blue[] 		= "#709abe";
static const char b_magenta[]	= "#af85bb";
static const char b_cyan[]  	= "#8abeb7";
static const char b_white[] 	= "#c5c8c6";

/* special colors */
static const char background[]  = "#1d1f21";
static const char foreground[] 	= "#d7d7d7";
