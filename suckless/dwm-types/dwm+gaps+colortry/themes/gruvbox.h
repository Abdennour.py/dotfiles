static const char black[]       = "#1e2122";
static const char gray2[]       = "#282b2c"; // unfocused window border
static const char gray3[]       = "#5d6061";
static const char gray4[]       = "#282b2c";
static const char blue[]        = "#6f8faf";  // focused window border
static const char green[]       = "#89b482";
static const char red[]         = "#ec6b64";
static const char orange[]      = "#d6b676";
static const char yellow[]      = "#d1b171";
static const char pink[]        = "#cc7f94";
static const char col_borderbar[]  = "#1e2122"; // inner border

static const char col_1[]       = "#1e2122";
static const char col_2[]       = "#1e2122";
static const char col_3[]       = "#5d6061";
static const char col_4[]       = "#f64645";
static const char col_cyan[]        = "#6f8faf";
